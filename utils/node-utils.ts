import { INode, IParent, Breadcrumb, Bundle } from '@minidoc/core/lib';
import Enumerable from 'linq';

export function hasChildren(node: INode): boolean {
    return (node as IParent).children !== undefined;
}

export function getName(node: INode): string | undefined {
    return node.document?.title || node.scopedId;
}

export function sortNodes(...nodes: INode[]): INode[] {
    return Enumerable.from(nodes)
        .orderByDescending(node => hasChildren(node) ? 1 : 0)
        .thenBy(node => getName(node)?.toLowerCase())
        .toArray();
}

export function isActivatedBreadcrumb(node: INode, breadcrumbs: Breadcrumb[]): boolean {
    return breadcrumbs.some(breadcrumb => breadcrumb.id === node.id);
}

export function lookupNode(id: string, bundle: Bundle): INode | null {
    const lookup = (node: INode): INode | null => {

        if (node.id === id) {
            return node;
        }

        if (hasChildren(node)) {
            const parent = node as IParent;

            for (const child of parent.children) {
                const match = lookup(child);

                if (match !== null) {
                    return match;
                }
            }
        }

        return null;
    };

    return lookup(bundle);
}
