import { GetStaticProps, GetStaticPaths } from 'next';
import { Bundle, INode, IParent, Breadcrumb } from '@minidoc/core/lib';
import Head from 'next/head';
import DocumentContent from '../components/DocumentContent';
import styles from '../styles/NodeDetail.module.scss';
import Link from 'next/link';
import { getNodes, getSerializableBundle } from '../services/bundle-service';
import { FolderIcon, FolderOpenIcon, DocumentIcon, ChevronRightIcon, ChevronDownIcon } from '@heroicons/react/outline';
import { hasChildren, sortNodes, getName, isActivatedBreadcrumb, lookupNode } from '../utils/node-utils';

const separator = '/';

type Props = {
    id: string;
    bundle: Bundle;
};

const NodeDetail = ({ id, bundle }: Props): JSX.Element => {
    const node: INode | null = lookupNode(id, bundle);

    if (!node) {
        return (<p>Node is not found, or invalid.</p>);
    }

    const document = node.document;

    const children = hasChildren(node) ?
        sortNodes(...(node as IParent).children) :
        [];
    const groups = children.filter(node => hasChildren(node)) as IParent[];
    const pages = children.filter(node => !hasChildren(node));

    return (
        <div className="grid grid-rows-2 min-h-screen divide-y divide-themed" style={{ gridTemplateRows: 'auto 1fr' }}>
            <Head>
                <title>{document?.title || node.scopedId || 'MiniDoc'}</title>
                <link rel="icon" href="./favicon.png" />

                {/* <link rel="stylesheet"
                    href="https://cdnjs.cloudflare.com/ajax/libs/github-markdown-css/4.0.0/github-markdown.min.css"
                    integrity="sha512-Oy18vBnbSJkXTndr2n6lDMO5NN31UljR8e/ICzVPrGpSud4Gkckb8yUpqhKuUNoE+o9gAb4O/rAxxw1ojyUVzg=="
                    crossOrigin="anonymous" referrerPolicy="no-referrer" /> */}

                {/* <base href="https://envize.gitlab.io/minidoc/site-gen-test/"></base> */}
                {/* <base href="http://127.0.0.1:5500/out/"></base> */}
            </Head>

            <header className="h-20 overflow-hidden">
                <div className="max-w-screen-xl mx-auto max-h-20">
                    <div className="w-sidenav h-20 flex">
                        <div className="mx-auto relative">
                            <div className={styles.blobBackdrop}>
                                <div className="-top-4 -left-8 bg-purple-300 dark:bg-purple-900" />
                                <div className="top-0 left-12 bg-yellow-300 dark:bg-yellow-900" />
                                <div className="top-8 left-0 bg-pink-300 dark:bg-pink-900" />
                            </div>
                            <div className="flex items-center">
                                <img src="./avatar.png" alt="MiniDoc" className="h-12 my-4 drop-shadow-md cursor-pointer" />
                                <p className="text-pink-500 font-light text-3xl ml-4">Docs</p>
                            </div>
                        </div>
                    </div>
                </div>
            </header>

            <div>
                <div className="max-w-screen-xl mx-auto flex w-full divide-x divide-themed min-h-full">
                    {SideNav(bundle, node.breadcrumbs)}
                    <main className="flex-1 pl-16 py-8 space-y-8">
                        {Breadcrumbs(node.breadcrumbs)}

                        <h1 className={styles.title}>{getName(node) || 'MiniDoc'}</h1>

                        {Groups(groups)}

                        {Pages(pages)}

                        {
                            (groups.length > 0 || pages.length > 0) &&
                            document &&
                            (document.general || document.story || document.happyFlow || document.alternateFlows.length > 0) &&
                            <hr className="-ml-16 mt-6 border-themed" />
                        }

                        {document && <DocumentContent document={document} />}
                    </main>
                </div>
            </div>
        </div>
    );
};

export default NodeDetail;

function SideNav(bundle: Bundle, breadcrumbs: Breadcrumb[]) {
    const rootNodes = sortNodes(...bundle.children);
    console.log(rootNodes);
    return (
        <nav className={`${styles.sidenav} w-sidenav relative`}>
            <h2 className="pt-8 pb-4 text-xl font-bold tracking-wide">Table of contents</h2>
            <ul className="divide-y divide-themed">
                {
                    rootNodes.map(child => (
                        <li key={child.id} className="py-4">
                            <Link href={convertIdToUrl(child.id)} passHref={true}>
                                <div className={`${styles.rootNavItem} flex cursor-pointer`}>
                                    <div className={`${styles.iconWrapper} flex`}>
                                        {getNavIcon(child, breadcrumbs)}
                                        <FolderOpenIcon className="h-6 w-6 mr-6 text-primary-500 inline-block" />
                                    </div>
                                    <span className="font-semibold tracking-wide inline-block">{getName(child)}</span>
                                </div>
                            </Link>
                            {hasChildren(child) && isActivatedBreadcrumb(child, breadcrumbs) && (
                                <div className="ml-6 mt-4">
                                    <div className="ml-6">
                                        {Menu(child, breadcrumbs)}
                                    </div>
                                </div>
                            )}
                            {/* {getNavIcon(child, breadcrumbs)}
                            <div>
                                <Link href={convertIdToUrl(child.id)} passHref={true}>
                                      <span className="font-semibold tracking-wide">{getName(child)}</span>
                                </Link>
                                {hasChildren(child) && isActivatedBreadcrumb(child, breadcrumbs) && (
                                    <div className="-ml-6 mt-4">
                                        {Menu(child, breadcrumbs)}
                                    </div>
                                )}
                            </div> */}
                        </li>
                    ))
                }
            </ul>
        </nav>
    );
}

function Breadcrumbs(breadcrumbs: Breadcrumb[]) {
    const getBreadcrumbName = (breadcrumb: Breadcrumb, index: number) => {
        return (index === 0 ? 'Home' : null) ?? breadcrumb.description ?? /[^/]*$/.exec(breadcrumb.id)?.toString();
    };

    return breadcrumbs.length > 1 ? (
        <div className={styles.breadcrumbs}>
            <ul className="px-0 space-x-8 list-none">
                {breadcrumbs.map((breadcrumb, index, array) => (
                    <li key={breadcrumb.id}
                        className="inline tracking-wider uppercase text-gray-600 dark:text-gray-200">
                        {index < array.length - 1 ?
                            <Link href={convertIdToUrl(breadcrumb.id)} passHref={true}>
                                {getBreadcrumbName(breadcrumb, index)}
                            </Link> :
                            <span>{getBreadcrumbName(breadcrumb, index)}</span>
                        }
                    </li>
                ))}
            </ul>
            <hr className="-ml-16 mt-6 border-themed" />
        </div>
    ) : null;
}

function convertIdToUrl(id: string) {
    return '.' + (id.startsWith('/') ? id : `/${id}`);
}

function Groups(groups: IParent[]) {
    const createPreviewItem = (node: INode) => (
        <li key={node.id} className="whitespace-nowrap overflow-hidden overflow-ellipsis">
            {getName(node)}
        </li>
    );

    return groups.length > 0 ? (
        <div>
            <ul className="groups grid grid-cols-2 gap-6">
                {groups.map(group => (
                    <Link key={group.id} href={convertIdToUrl(group.id)} passHref={true}>
                        <li className="rounded-lg border border-themed px-8 py-4 hover:shadow-lg transition-shadow cursor-pointer">
                            <div className="flex items-center">
                                <FolderIcon className="h-7 mr-6 text-primary-500" />
                                <h2 className="font-bold text-xl">{getName(group)}</h2>

                            </div>
                            <ul className="mt-4 grid grid-cols-2 grid-rows-3 gap-2 grid-flow-col">
                                {group.children.slice(0, 6).map(createPreviewItem)}
                            </ul>
                        </li>
                    </Link>
                ))}
            </ul>
        </div>
    ) : null;
}

function Pages(pages: INode[]) {
    return pages.length > 0 ? (
        <div>
            <p className="pb-4 text-xl font-bold tracking-wide">Pages</p>
            <ul className="pages grid grid-cols-3 gap-6">
                {pages.map(page => (
                    <Link key={page.id} href={convertIdToUrl(page.id)} passHref={true}>
                        <li className="rounded-lg border border-themed px-8 py-4 hover:shadow-lg transition-shadow cursor-pointer">
                            <div className="flex items-center">
                                <DocumentIcon className="h-7 mr-6 text-primary-500" />
                                <h2 className="whitespace-nowrap overflow-hidden overflow-ellipsis">{getName(page)}</h2>
                            </div>
                        </li>
                    </Link>
                ))}
            </ul>
        </div>
    ) : null;
}

function getNavIcon(child: INode, breadcrumbs: Breadcrumb[]) {
    if (hasChildren(child) && child.id === breadcrumbs[1]?.id) {
        return (<FolderOpenIcon className="h-6 w-6 mr-6 text-primary-500 inline-block" />);
    } else if (hasChildren(child)) {
        return (<FolderIcon className="h-6 w-6 mr-6 text-primary-500 inline-block" />);
    }

    return (<DocumentIcon className={`${styles.isPage} h-6 w-6 mr-6 text-primary-500 inline-block`} />);
}

function getSubNavIcon(child: INode, breadcrumbs: Breadcrumb[]) {
    if (hasChildren(child) && breadcrumbs.some(breadcrumb => breadcrumb.id === child.id)) {
        return (<ChevronDownIcon className="h-4 my-1 text-primary-500" />);
    } else if (hasChildren(child)) {
        return (<ChevronRightIcon className="h-4 my-1 text-primary-500" />);
    }

    return null;
}

function Menu(node: INode, breadcrumbs: Breadcrumb[]) {
    const children = hasChildren(node) ?
        sortNodes(...(node as IParent).children) :
        [];

    return hasChildren(node) ? (
        <ul>
            {children
                .map(child => (
                    <li key={child.id} className="my-1 flex">
                        <div>
                            <Link href={convertIdToUrl(child.id)} passHref={true}>
                                <div className="flex -ml-6 cursor-pointer">
                                    <div className="w-6 flex">
                                        {getSubNavIcon(child, breadcrumbs)}
                                    </div>
                                    <div className="transform hover:translate-x-2 transition-transform">
                                        <span className={isActivatedBreadcrumb(child, breadcrumbs)
                                            ? 'font-semibold tracking-wide'
                                            : ''}>
                                            {getName(child)}
                                        </span>
                                    </div>
                                </div>
                            </Link>
                            <div className="ml-6">
                                {isActivatedBreadcrumb(child, breadcrumbs) && Menu(child, breadcrumbs)}
                            </div>
                        </div>
                    </li>
                ))}
        </ul>
    ) : null;
}

export const getStaticPaths: GetStaticPaths = async () => {
    const nodes = getNodes();

    const getIds = (node: INode) => {
        const ids = node.id.split(separator);

        while (ids[0] === '') {
            ids.shift();
        }

        // if (hasChildren(node)) {
        //     ids.push('index');
        // }

        console.log(ids);

        return ids;
    };

    const paths = nodes
        .map(node => ({ params: { ids: getIds(node) } }));

    return {
        paths: paths,
        fallback: false
    };
};

export const getStaticProps: GetStaticProps<Props> = async (context) => {
    // @ts-expect-error no custom type declared for params
    const ids: string[] = context.params.ids ?? ['/'];
    const id: string = ids.join(separator);

    const bundle = getSerializableBundle();

    return {
        props: {
            id: id,
            bundle: bundle
        }
    };
};
