import { INode, Bundle, IParent } from '@minidoc/core/lib';
import { getBundle as parseBundle } from '@minidoc/bundler/lib';

let cachedBundle: Bundle | undefined = undefined;
let cachedSerializableBundle: Bundle | undefined = undefined;
let cachedNodes: INode[] | undefined = undefined;

export function getBundle(): Bundle {
    // @ts-expect-error documents_path not declared in ts
    const rootDirectory: string = process.env.DOCUMENTS_PATH;

    cachedBundle ??= parseBundle(rootDirectory);

    return cachedBundle;
}

export function getSerializableBundle(): Bundle {
    const bundle = getBundle();
    cachedSerializableBundle ??= JSON.parse(JSON.stringify(bundle)) as Bundle;

    return cachedSerializableBundle;
}

export function getNodes(): INode[] {
    const bundle = getBundle();
    cachedNodes ??= flattenNode(bundle);

    return cachedNodes;
}

function flattenNode(node: INode) {
    const nodes: INode[] = [];

    nodes.push(node);

    const isParent = (node as IParent).children !== undefined;

    if (isParent) {
        const parent = node as IParent;

        for (const child of parent.children) {
            nodes.push(...flattenNode(child));
        }
    }

    return nodes;
}
