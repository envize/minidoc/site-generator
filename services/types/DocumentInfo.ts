import { Document } from '@minidoc/core/lib';

const seperator = '/';

export default class DocumentInfo {
    public nodes: string[];
    constructor(
        public document: Document,
        public id: string,
    ) {
        this.nodes = id.split(seperator);
    }
}
