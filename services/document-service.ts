import * as fs from 'fs';
import * as pathUtils from 'path';
import { process as processDocument } from '@minidoc/processor/lib';
import { Document } from '@minidoc/core/lib';
import DocumentInfo from './types/DocumentInfo';

// @ts-expect-error documents_path not declared in ts
const rootDirectory: string = process.env.DOCUMENTS_PATH;

const extensions = ['.mdoc', '.minidoc'];
const seperator = '/';

export function getDocument(id: string): Document | null {
    if (id === undefined || id === null) {
        return null;
    }

    const path = getDocumentPath(id);
    if (path === null) {
        return null;
    }

    const fileContent = fs.readFileSync(path).toString();

    let doc: Document | null = null;

    try {
        doc = processDocument(fileContent);
    // eslint-disable-next-line no-empty
    } catch { }

    return doc;
}

function getDocumentPath(id: string): string | null {
    const paths = extensions.map(extension => pathUtils.join(rootDirectory, `${id}${extension}`));

    for (const path of paths) {
        if (fs.existsSync(path)) {
            return path;
        }
    }

    return null;
}

function getFilenames(path: string, parentNodes: string[] = []): string[] {
    const filenames: string[] = [];

    // get all files and directories in directory
    for (const obj of fs.readdirSync(path, { withFileTypes: true })) {
        const nodes = [...parentNodes, obj.name];

        if (obj.isDirectory()) {

            // when directory > join path, and call getFilenames recursively
            const concatPath = pathUtils.join(path, obj.name);
            const filenamesInSubDirectory = getFilenames(concatPath, nodes);
            filenames.push(...filenamesInSubDirectory);

        } else if (obj.isFile() &&
            extensions.some(extension => obj.name.endsWith(extension))) {

            // when file, and is minidoc file > add to file result
            const filename = nodes.join(seperator);
            filenames.push(filename);
        }
    }

    return filenames;
}

export function getAllDocuments(): DocumentInfo[] {
    const regex = new RegExp(/.*(?=\.)/);
    const filenames = getFilenames(rootDirectory);

    // @ts-expect-error the filter call above ensures regex match
    const ids = filenames.map(filename => regex.exec(filename).toString());

    const result: DocumentInfo[] = [];

    for (const id of ids) {
        const doc = getDocument(id);

        if (doc !== null) {
            const info = new DocumentInfo(doc, id);
            result.push(info);
        }
    }

    return result;
}
