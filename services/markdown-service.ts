import { Markdown } from "@minidoc/core";
import { Converter } from "showdown";

const converter = new Converter(
    {
        simpleLineBreaks: false,
        simplifiedAutoLink: true,
        tables: true,
        tasklists: true,
        requireSpaceBeforeHeadingText: true,
        emoji: true,
        openLinksInNewWindow: true
    }
);

export function toHtml(markdown: Markdown): string {
    return converter.makeHtml(markdown.content);
}
