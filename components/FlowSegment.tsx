import { Flow } from "@minidoc/core";
import styles from '../styles/FlowSegment.module.scss';
import Graph from "./Graph";

type Props = {
    flow: Flow;
    flowsToGraph: Flow[];
};

export default function FlowSegment({ flow, flowsToGraph }: Props): JSX.Element {
    return (
        <div className={styles.FlowSegment}>
            {flow.description ? <h2>{flow.description}</h2> : null}
            <div className={styles.horizontal}>
                <section>
                    {Table(flow)}
                </section>
                <aside>
                    <Graph flows={flowsToGraph}></Graph>
                </aside>
            </div>
        </div>
    );
}

function Table(flow: Flow) {
    return (
        <table className="w-full">
            <tbody>
                {flow.start ? (
                    <tr>
                        <td colSpan={2}>comes from</td>
                        <td>{flow.start.target.description}</td>
                    </tr>
                ) : null}

                {flow.steps.map(step => (
                    <tr key={step.id}>
                        <td>{step.position + 1}.</td>
                        <td>{step.type}</td>
                        <td>{step.description}</td>
                    </tr>
                ))}

                {flow.end ? (
                    <tr>
                        <td colSpan={2}>returns to</td>
                        <td>{flow.end.target.description}</td>
                    </tr>
                ) : null}
            </tbody>
        </table>
    )
}