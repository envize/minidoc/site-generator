import { Markdown } from "@minidoc/core";
import { toHtml } from "../services/markdown-service";

type Props = {
    title?: string;
    content: Markdown;
};

export default function MarkdownSegment({ title, content }: Props): JSX.Element {
    return (
        <div>
            {content ? (
                <div>
                    {title !== undefined ? <h1>{title}</h1> : null}

                    <div
                        className="markdown-content"
                        dangerouslySetInnerHTML={{ __html: toHtml(content) }}>
                    </div>
                </div>
            ) : null}
        </div>
    );
}