import { Flow } from "@minidoc/core";
import { plot } from "@minidoc/mermaid";
import mermaid from "mermaid";
import hash from 'object-hash';
import { useEffect } from "react";
import styles from '../styles/Graph.module.scss';

type Props = {
    flows: Flow[];
};

export default function Graph({ flows }: Props): JSX.Element {
    const graph = plot(flows);
    const key = hash(graph);

    useEffect(() => {
        mermaid.init(`.${styles.mermaid}.key-${key}`);
    });

    return (
        <div key={key} className={`${styles.mermaid} mermaid key-${key}`}>
            { graph }
        </div>
    );
}
