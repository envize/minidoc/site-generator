import { Document, Flow, Markdown } from '@minidoc/core';
import styles from '../styles/DocumentContent.module.scss';
import MarkdownSegment from "./MarkdownSegment";
import FlowSegment from "./FlowSegment";
import Graph from "./Graph";

type Props = {
    document: Document;
};

export default function DocumentContent({ document }: Props): JSX.Element {
    return (
        <div>
            {document ? (
                <div>
                    <article className={`${styles.DocumentContent} my-markdown-body`}>
                        {Content(document)}

                        <p>Generated with &lt;3 by MiniDoc.</p>
                    </article>
                    {/* <article className={`${styles.DocumentContent} markdown-body`}>
                        {Content(document)}

                        <p>Generated with &lt;3 by MiniDoc.</p>
                    </article> */}
                </div>
            ) : null}
        </div>
    );
}

function Content(document: Document) {
    const shouldShowAllFlowsDiagram =
        document.happyFlow && (
            document.alternateFlows.length > 0 // or exceptions
        );

    return (
        <div>
            {/* // todo: no null checks on general and story */}
            <MarkdownSegment content={document.general as Markdown}></MarkdownSegment>
            <MarkdownSegment title="User story" content={document.story as Markdown}></MarkdownSegment>

            {document.happyFlow ? (
                <div>
                    <h1>Happy flow</h1>
                    <FlowSegment
                        flow={document.happyFlow}
                        flowsToGraph={[document.happyFlow]}></FlowSegment>
                </div>
            ) : null}

            {document.alternateFlows.length > 0 ? (
                <div>
                    <h1>Alternate flow{document.alternateFlows.length > 1 ? 's' : ''}</h1>
                    {document.alternateFlows.map((flow, i) => (
                        <FlowSegment
                            key={i}
                            flow={flow}
                            // todo: no null check on happy flow
                            flowsToGraph={[flow, document.happyFlow as Flow]}></FlowSegment>
                    ))}
                </div>
            ) : null}

            {shouldShowAllFlowsDiagram ? (
                <div>
                    <h1>All flows</h1>
                    <div className="allFlowsDiagram">
                        {/* // todo: no null check on happy flow */}
                        <Graph flows={[document.happyFlow as Flow, ...document.alternateFlows]}></Graph>
                    </div>
                </div>
            ) : null}
        </div>
    );
}
