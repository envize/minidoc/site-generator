// eslint-disable-next-line @typescript-eslint/no-var-requires
const colors = require('tailwindcss/colors');

module.exports = {
    purge: [
        './pages/**/*.{js,ts,jsx,tsx}',
        './components/**/*.{js,ts,jsx,tsx}'
    ],
    mode: 'jit',
    darkMode: 'media', // or 'media' or 'class'
    theme: {
        extend: {
            colors: {
                primary: colors.purple,
                secondary: colors.amber
            },
            width: {
                'sidenav': '20rem'
            },
            zIndex: {
                '-1': '-1',
            },
            animation: {
                blob: "blob 7s infinite"
            },
            // keyframes: {
            //     blob: {
            //         "0%": {
            //             transform: "translate(0, 0) scale(1)"
            //         },
            //         "33%": {
            //             transform: "translate(2rem, -2rem) scale(1.1)"
            //         },
            //         "66%": {
            //             transform: "translate(-2.5rem, 2rem) scale(1.2)"
            //         },
            //         "100%": {
            //             transform: "translate(0, 0) scale(1)"
            //         }
            //     }
            // },
            keyframes: {
                blob: {
                    "0%": {
                        transform: "translate(0, 0) scale(1)"
                    },
                    "25%": {
                        transform: "translate(2rem, -2rem) scale(1.1)"
                    },
                    "50%": {
                        transform: "translate(-2.5rem, 0) scale(1.2)"
                    },
                    "75%": {
                        transform: "translate(-2.5rem, -3rem) scale(.9)"
                    },
                    "100%": {
                        transform: "translate(0, 0) scale(1)"
                    }
                }
            },
            listStyleType: {
                square: 'square',
                circle: 'circle'
            }
        }
    },
    variants: {
        extend: {
            opacity: ['dark'],
            // text: [ 'dark' ]
        },
    },
    plugins: [
        function ({ addBase, theme }) {
            function extractColorVars(colorObj, colorGroup = '') {
                return Object.keys(colorObj).reduce((vars, colorKey) => {
                    const value = colorObj[colorKey];

                    const newVars =
                        typeof value === 'string'
                            ? { [`--color${colorGroup}-${colorKey}`]: value }
                            : extractColorVars(value, `-${colorKey}`);

                    return { ...vars, ...newVars };
                }, {});
            }

            addBase({
                ':root': extractColorVars(theme('colors')),
            });
        }
    ]
};
